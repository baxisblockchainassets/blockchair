## Blockchair

The following codes are experimental, to explore the content of blockchair api.

Documentation: https://baxisblockchainassets.atlassian.net/l/c/tEV0cKmk

---

## Access to Binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fbaxisblockchainassets%2Fblockchair%2Fsrc%2Fmaster/master)

## Requirements
	
	pip install schedule
	pip install requests

## Web API

	API Rest